import Vue from 'vue'
import TWEEN from 'tween'
require('./font-awesome.js')

var mapping = {
  'Early': 2,
  'Growth': 3,
  'Hot': 3,
  'Leader': 3,
  'Mature': 2,
  'Warm': 2,
  'Challenger': 2,
  'Decline': 1,
  'Cold': 1,
  'Startup': 1
}

function getMarketFac(a,b,c) {
  return (mapping[a] + mapping[b] + mapping[c])
}

function initState() {
  return {
    lifetime: 36,
    perRevPart: 0.6,
    costPerRep: 10000,
    avgSaleCycle: 90,
    repEffic: 0.93,
    rampupMN: 0.69444444,

    startWith: '1',

    marketMat: 'Growth',
    leadsProv: 'Hot',
    marketPos: 'Leader',

    annPartRec: 500,
    annRevPart: 3000000,
    avgMonRevPart: 800,

    avgRecPart: 0,
    repReq: 0,
    lifeVal: 0,
    roi: 0,

    showRes: false,

    annRevPartInp: '3,000,000',
    avgMonRevPartInp: '800',
    annPartRecInp: '500',

    animARP1: 0,
    animRR1: 0,
    animLV1: 0,
    animROI1: 0,
    animARP2: 0,
    animRR2: 0,
    animLV2: 0,
    animROI2: 0,
    animARPF: 0,
    animRRF: 0,
    animLVF: 0,
    animROIF: 0
  }
}

function animate () {
  if (TWEEN.update()) {
    requestAnimationFrame(animate)
  }
}

Number.prototype.numberFormat = function(decimals, dec_point, thousands_sep) {
    dec_point = typeof dec_point !== 'undefined' ? dec_point : '.';
    thousands_sep = typeof thousands_sep !== 'undefined' ? thousands_sep : ',';

    var parts = this.toFixed(decimals).split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_sep);

    return parts.join(dec_point);
}

var calc = new Vue({
  el: '#recruitCalc',
  data: function() {
    return initState()
  },
  methods: {
    calculate: function() {
      var marketFac = getMarketFac(this.marketMat,this.leadsProv,this.marketPos)
      if (this.startWith == '1') {
        this.annPartRec = this.annRevPart / (this.avgMonRevPart*this.perRevPart*12*this.rampupMN)
      }

      this.avgRecPart = Math.round(this.annPartRec / 12)
      this.repReq = Math.ceil((this.avgRecPart / marketFac) / this.repEffic)
      this.lifeVal = (this.annPartRec * this.perRevPart * this.avgMonRevPart * this.lifetime)
      this.roi = ((this.lifeVal - (10000 * this.repReq * 12)) / (9400 * this.repReq * 12))

      this.showRes = true
    },
    reset: function() {
      var initialData = initState()
      for (let prop in initialData) {
          this[prop] = initialData[prop];
      }
    },
    addCommas: function(e) {
      var aPRI = this.annPartRecInp.replace(/\D/g, "")
      var aRPI = this.annRevPartInp.replace(/\D/g, "")
      var aMRPI = this.avgMonRevPartInp.replace(/\D/g, "")

      this.annPartRec = parseInt(aPRI)
      this.annRevPart = parseInt(aRPI)
      this.avgMonRevPart = parseInt(aMRPI)

      this.annPartRecInp = aPRI.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      this.annRevPartInp = aRPI.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      this.avgMonRevPartInp = aMRPI.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    },
    removeCommas: function(e) {
      var el = e.target
      el.value = el.value.replace(/\D/g, "")
    }
  },
  watch: {
    avgRecPart: function(newValue, oldValue) {
      var vm = this
      new TWEEN.Tween({ tweeningNumber: oldValue })
        .easing(TWEEN.Easing.Quadratic.Out)
        .to({ tweeningNumber: newValue }, 1500)
        .onUpdate(function () {
          var tenPer = vm.avgRecPart * .1
          vm.animARP1 = (this.tweeningNumber - tenPer).numberFormat(0)
          vm.animARP2 = (this.tweeningNumber + tenPer).numberFormat(0)
          vm.animARPF = vm.animARP1 + " - " + vm.animARP2
          if (vm.animARP1 == vm.animARP2) {
            vm.animARPF = vm.animARP1
          }
        })
        .start()

      animate()
    },
    repReq: function(newValue, oldValue) {
      var vm = this
      new TWEEN.Tween({ tweeningNumber: oldValue })
        .easing(TWEEN.Easing.Quadratic.Out)
        .to({ tweeningNumber: newValue }, 1500)
        .onUpdate(function () {
          var tenPer = vm.repReq * .1
          vm.animRR1 = (this.tweeningNumber - tenPer).numberFormat(0)
          vm.animRR2 = (this.tweeningNumber + tenPer).numberFormat(0)
          vm.animRRF = vm.animRR1 + " - " + vm.animRR2
          if (vm.animRR1 == vm.animRR2) {
            vm.animRRF = vm.animRR1
          }
        })
        .start()

      animate()
    },
    lifeVal: function(newValue, oldValue) {
      var vm = this
      new TWEEN.Tween({ tweeningNumber: oldValue })
        .easing(TWEEN.Easing.Quadratic.Out)
        .to({ tweeningNumber: newValue }, 1500)
        .onUpdate(function () {
          var tenPer = vm.lifeVal * .1
          vm.animLV1 = (this.tweeningNumber - tenPer).numberFormat(0)
          vm.animLV2 = (this.tweeningNumber + tenPer).numberFormat(0)
          vm.animLVF = vm.animLV1 + " - " + vm.animLV2
          if (vm.animLV1 == vm.animLV2) {
            vm.animLVF = vm.animLV1
          }
        })
        .start()

      animate()
    },
    roi: function(newValue, oldValue) {
      var vm = this
      new TWEEN.Tween({ tweeningNumber: oldValue })
        .easing(TWEEN.Easing.Quadratic.Out)
        .to({ tweeningNumber: newValue }, 1500)
        .onUpdate(function () {
          var tenPer = vm.roi * .1
          vm.animROI1 = (this.tweeningNumber - tenPer).numberFormat(0)
          vm.animROI2 = (this.tweeningNumber + tenPer).numberFormat(0)
          vm.animROIF = vm.animROI1 + " - " + vm.animROI2
          if (vm.animROI1 == vm.animROI2) {
            vm.animROIF = vm.animROI1
          }
        })
        .start()

      animate()
    }
  }
})
